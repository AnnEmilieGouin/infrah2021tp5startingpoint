CREATE DATABASE usersDB;
use usersDB;

CREATE TABLE users (
	username VARCHAR(32) NOT NULL,
	isInitialUser BIT DEFAULT 0 NOT NULL,
	UNIQUE(username)
);

INSERT INTO users (
	username,
	isInitialUser
)
VALUES
	(Bob, 1),
	(Body, 1),
	(Bill, 0),
	(Joe, 0),
	(Jack, 0)
