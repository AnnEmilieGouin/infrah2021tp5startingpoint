#!/bin/bash

docker stop tp5dsaeg
docker container rm tp5dsaeg
docker image rm tp5dsaeg_image
docker volume rm tp5dsaeg_vol

docker volume create --name tp5dsaeg_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp5dsaeg_image -f ./project/docker/DockerfileAPI .
docker run -d -p 5555:5555 --mount source=tp5dsaeg_vol,target=/mnt/app/ --name tp5dsaeg -e "INFRA_TP5_DB_HOST=db-tp5-infra.ddnsgeek.com" -e "INFRA_TP5_DB_PORT=7777" -e "INFRA_TP5_DB_USER=produser" -e "INFRA_TP5_DB_PASSWORD=3ac4d0b0e24871436f45275890a458c6" -e "INFRA_TP5_DB_TYPE=MYSQL" tp5dsaeg_image

