#!/bin/bash

printf "Running unit tests:\n"
python3 -m unittest discover -s project -v
if [ $? -ne 0 ]; then
    echo "Unit tests failed"
    exit 1
fi

sh deployLocal.sh

sleep 5
printf '\nRunning deployment tests:\n'
python3 -m unittest discover -s project -v -p dtest_*.py
if [ $? -ne 0 ]; then
    echo "Deployment tests failed"
    exit 1
fi
